# Manipulate_Spatial_Data_R

The R file code_carto contains the main code to manipulate spatial data with R. You will need the different files that contains data (coordonnees_geo, finess_geoloc, sae, and hdf files). 

There are two gif results that were produced and you can see them in the files : all_numbers_gif and nb_etp_inf_autres_gif. 

The HTML file carte_R contains the questions that I had to answer/produce with R code :

Q1 - Update the code above to print the map with thicker and red borders

Q2 - Make a spatial object cities_points from cities dataset (crs = 4326)

Q3 - Place the cities from cities dataset on the hdf base map

Q4 - Make a map with areas coloured depending on the _nbplaces variables

Q5 - Update your last code to customise the map

Q6 - Add the dots from finess data to your last map

Q7 - Update the code above to make facet according to the following column : _NAME2

Q8 - Update the code above to add facets for the following variables: _nb_nonmedical and _nb_etp_inf_etautres

Q9 - Add to you last map the names of the variables in headers, make 2 rows and 3 columns

Q10 - Try to customise some of the previous maps


Author Marion Estoup

E-mail : marion_110@hotmail.fr

March 2023

